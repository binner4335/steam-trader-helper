"use strict";

export class ExtensionManager {
	constructor(offers) {
		this.offers = offers;
		this.init();
	}
	init() {
		this.offers.checkAllHealthAndReload()
		this.onUserSendAction();
		this.onUserReloginOnSites();
		this.modifyRefererHeaderForTradeOfferCreating();
	}
	onUserSendAction() {
		// On User Clicked Popup Action
		chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
		    if(message.method === 'reload'){
		        (async () => {
		            sendResponse(await this.offers.checkAllHealthAndReload());
		        })();
		    }
		    return true;
		});
	}
	onUserReloginOnSites() {
		// On User
		chrome.cookies.onChanged.addListener(async (changeInfo) => {
		    //logged in Steam
		    if(changeInfo.cookie.name === 'steamLoginSecure'
		    && changeInfo.cause === 'explicit') {
		    	setTimeout(async () => {
		    		let res = await this.offers.checkAllHealthAndReload();
		    	}, 10000);
		    }
		    //logged in Trader
		    if(changeInfo.cookie.name === 'sid'
		    && changeInfo.cause === 'explicit') {
		    	setTimeout(async () => {
		    		await this.offers.checkTraderHealthAndReload();
		    	}, 10000);
		    }
		});
	}
	modifyRefererHeaderForTradeOfferCreating() {
		//Steam требует заголовок Referer со значением - трейд ссылкой партнера для создания трейд оффера. Просто передача этого заголовка в параметры объекта fetch н работает, т.к. расширение хроме перехватывает и модифицирует этот заголовок. Поэтому нужно модифицировать его здесь явно.
		chrome.webRequest.onBeforeSendHeaders.addListener((details) => {
			// Заголовок "X-Referer" со значенем - трейд ссылкой партнера передается из SteamAPI.js метода createTradeOffer и заменяется в текущем методе на "Referer"
			const newReferer = details.requestHeaders.reduce((acc, nextHeader) => nextHeader.name === 'X-Referer' ? nextHeader.value : acc);
		    let gotReferer = false;
		    for(let n in details.requestHeaders){
		        gotReferer = details.requestHeaders[n].name.toLowerCase() === "referer";
		        if(gotReferer){
		            details.requestHeaders[n].value = newReferer;
		            break;
		        }
		    }
		    if(!gotReferer){
		        details.requestHeaders.push({name:"Referer",value:newReferer});
		    }
		    return { requestHeaders: details.requestHeaders };
		},{
		    urls:["https://steamcommunity.com/tradeoffer/new/*"]
		},[
		    "requestHeaders",
		    "blocking",
		    "extraHeaders"
		]);
	}
}