"use strict";

import { WSClient, WSMsg, WSMsgType } from './websocket.js';

export class TraderAPI { 
	constructor(httpAddr, wsAddr) 
	{
		this.httpAddr = httpAddr;
		this.wsAddr = wsAddr;
	}
	async isAuthedWithSteamid(steamid) 
	{
		let response = await fetch(`${ this.httpAddr }/api/getsteamid`);

		if(response.ok === false) {
			return false;
		}
		let json = await response.json(); 

		return json.success !== undefined && json.data == steamid;
	}
	async isSteamApiKeySaved(key) 
	{
		const response = await fetch(`${ this.httpAddr }/api/getUserSteamApiKey`);
		if(response.ok === false) {
			return false;
		}
		const json = await response.json(); 

		return json.success != null && json.data == key;
	}

	async saveSteamApiKeyOnTraderAccount(key)
	{
		const url = `${ this.httpAddr }/api/SetUserSteamApiKey/?steamApiKey=${key}`;

		try {
			const response = await fetch(url);

			const text = await response.text();
			
			const json = JSON.parse(text);

			return json.success;
		} catch(e) {
			console.error(e.message);
			return false;
		}
	}

	async InitWSIfNotAndAddTradeMsgListeners(sendOfferCb, cancelOfferCb)
	{
		let self = this;

		if(!WSClient.connected) {
			initWS();
		}

		function initWS() 
		{
			WSClient.DeleteAllHandlers();

			WSClient.AddHandler(WSMsgType.Auth, async function(msg) {
				if(msg == "0")
					await trySendWSAuthMsg();
			}, false);

			WSClient.AddHandler(WSMsgType.SendP2pOffer, sendOfferCb);
			WSClient.AddHandler(WSMsgType.CancelP2pOffer, cancelOfferCb);

			WSClient.Connect(self.wsAddr, async function() {
				await trySendWSAuthMsg();
			});
		}

		async function getWSToken() 
		{
			let response = await fetch(`${ self.httpAddr }/api/getwstoken`);

			if(response.ok === false) {
				return false;
			}

			return await response.json();
		}

		async function trySendWSAuthMsg() 
		{
			try {
				const wsToken = await getWSToken();

				if(wsToken) {
					WSClient.Send(new WSMsg(WSMsgType.Auth, wsToken));
				}
			}
			catch(e) {
				console.error('trySendWSAuthMsg', e);
			}
		}
	}
	isWSConnected() {
		return WSClient.connected;
	}
	async getLastExtensionVersion()
	{
		let response = await fetch(`${ this.httpAddr }/api/p2pTradesChromeExtensionLastVersion`);

		if(response.ok === false) {
			return false;
		}

		const json = await response.json();

		return json.version;
	}
	async notifyBackendAboutOfferCreatingResult(result) 
	{
		if(!result) return;
		const url = this.httpAddr + '/api/OnP2POfferCreated/?offerId='+result.tradeofferid+'&offerMsg='+result.message;
		const response = await fetch(url);
		const json = await response.json();
		return json;
	}
	async notifyBackendAboutOfferSendingResult(result) 
	{
		if(!result) return;
		const url = this.httpAddr + '/api/OnP2POfferSent/?offerId='+result.tradeofferid+'&offerMsg='+result.message;
		const response = await fetch(url);

		return await response.json();
	}
	async notifyBackendAboutOfferChangedStatus(result) 
	{
		if(!result) return;
		const url = this.httpAddr + '/api/OnP2POfferChangedStatus/?offerId='+result.tradeofferid;
		const response = await fetch(url);

		return await response.json();
	}
}