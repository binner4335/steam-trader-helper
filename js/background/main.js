"use strict";

const traderHttpAddr = 'https://steam-trader.com', traderWSAddr = "wss://ws.steam-trader.com";
const version = chrome.runtime.getManifest().version

import { SteamAPI } from './SteamAPI.js';
import { TraderAPI } from './TraderAPI.js';
import { HealthChecker } from './HealthChecker.js';
import { TradeOfferManager } from './TradeOfferManager.js';
import { ExtensionManager } from './ExtensionManager.js';

const steam = new SteamAPI();
const trader = new TraderAPI(traderHttpAddr, traderWSAddr);
const health = new HealthChecker(steam, trader, version);
const offers = new TradeOfferManager(steam, trader, health);
const extension = new ExtensionManager(offers);