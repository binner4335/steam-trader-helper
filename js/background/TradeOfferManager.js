"use strict";

import { Utils } from './Utils.js';
import { SteamTradeOfferState } from './SteamTradeOfferState.js';
import { RateLimitedPromise } from './RateLimitedPromise.js';

const throttle5s = RateLimitedPromise(5000);

export class TradeOfferManager {
	constructor(steam, trader, health) 
	{
		this.steam = steam;
		this.trader = trader;
		this.health = health;
		this.listeningForUpdateOffers = {};
	}
	async checkAllHealthAndReload() 
	{
		await this.health.check();

		return await this.reload(this);
	}
	async checkSteamHealthAndReload() 
	{
		await this.health.checkSteam();
		
		return await this.reload(this);
	}
	async checkTraderHealthAndReload() 
	{
		await this.health.checkTrader();
		
		return await this.reload();
	}

	async reload() 
	{
		this.trader.InitWSIfNotAndAddTradeMsgListeners(
			this.onCreateOfferMsgFromTrader.bind(this),
			this.onCancelOfferMsgFromTrader.bind(this)
		);

	    return this.health.getInfo();
	}
	async onCreateOfferMsgFromTrader(msg) 
	{
    	if(!this.health.allIsOk()) {
    		await Utils.sendAlertToUserUi("p2p Торговля на вашем аккаунте не работает - " + this.health.nextError());
            return false;
        }
		if(await this.steam.isOfferAlreadyCreated(msg.assetIds)) {
            await Utils.sendAlertToUserUi("Предложение обмена уже создано. Проверьте стим.");
            return false;
        }
		const createRes = await this.steam.createTradeOffer(msg);
        if(createRes === false) {
        	let strError = "При отправке этого предложения обмена произошла ошибка. Пожалуйста, повторите попытку."
        	await this.health.check();
        	const healthInfo = this.health.getInfo();
        	if(!healthInfo.allRight) {
        		if (!healthInfo.checkList.steamAccountIsAuthorized) {
	        		strError = "Ошибка. Авторизуйтесь на сайте steamcommunity.com";
	        	} else if (!healthInfo.checkList.traderAccountIsAuthorized) {
	        		strError = "Ошибка. Авторизуйтесь на сайте steam-trader.com";
	        	}
        	}
        	await Utils.sendAlertToUserUi(strError);
        } else if(createRes.strError != undefined) {
            const strError = createRes.strError;
            await Utils.sendAlertToUserUi(strError);
            await this.health.check();
        } else {
            let sendRes = await this.trader.notifyBackendAboutOfferCreatingResult(createRes);
            this.listenOfferStatusChangesAndNotifyTrader(createRes);
        }
	}
	async onCancelOfferMsgFromTrader(msg) 
	{
		const offer_id = msg.steamOfferId;
    	const cancelRes = await this.steam.cancelTradeOffer(offer_id);
    	delete this.listeningForUpdateOffers[offer_id];
	}
	async listenOfferStatusChangesAndNotifyTrader(offer)
	{
		offer.isBackendNotified = false;
		const offer_id = offer.tradeofferid;

		this.listeningForUpdateOffers[offer_id] = offer;

		while(this.listeningForUpdateOffers[offer_id] != null) 
		{
			try {
				await throttle5s();
				const traderOfferData = this.listeningForUpdateOffers[offer_id];
				const steamOfferData = await this.steam.getTradeOffer(offer_id);
				const steamOffer = steamOfferData.response.offer;

				if(
					steamOffer.trade_offer_state === SteamTradeOfferState.Active // 2
					&& 
					!traderOfferData.isBackendNotified
				) {
					const res = await this.trader.notifyBackendAboutOfferSendingResult(traderOfferData);
					if(res.success === true) {
						traderOfferData.isBackendNotified = true;
					}
				}

				if(
					![
						SteamTradeOfferState.Active, 
						SteamTradeOfferState.CreatedNeedsConfirmation
					].includes(steamOffer.trade_offer_state)
				) {
					await this.trader.notifyBackendAboutOfferChangedStatus(traderOfferData);
					delete this.listeningForUpdateOffers[offer_id];
				}
			} catch(err) {
				// логгировать ошибки на веб хук трейдера
				console.error(err);
			}
		}
	}
}
