import { SteamTradeOfferState } from './SteamTradeOfferState.js';
import { RateLimitedPromise } from './RateLimitedPromise.js';
import { Utils } from './Utils.js';

const throttle1s = RateLimitedPromise(1000);

export class SteamAPI {
	constructor() {
		this.accountIsAuthed = false;
		this.accountSteamid = null;
		this.accountApiKey = null;
	}
	async tryGetAuthedSteamid() 
	{
		const steamid = null;
		const response = await fetchRateLimited('https://steamcommunity.com');
		const text = await response.text(); 
		const matches = text.match(/g_steamID = "([0-9]+)";/);
		
		if(matches != null) {
			this.accountIsAuthed = true;
			this.accountSteamid = matches[1];
		} else {
			this.accountIsAuthed = false;
			this.accountSteamid = null;
		}
		return this.accountSteamid;
	}
	async isApiKeyRegistered() 
	{
		const response = await fetchRateLimited('https://steamcommunity.com/dev/apikey');
		return parseApiKey(response, this);
	}

	async isAccountAuthorized() 
	{
		const steamid = await this.tryGetAuthedSteamid();
		return steamid != null;
	}
	async createTradeOffer(data) 
	{
		const url = 'https://steamcommunity.com/tradeoffer/new/send',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				sessionid: sessionidCookie.value,
				serverid: 1,
				partner: data.partnerSteamid,
				json_tradeoffer: data.jsonTradeOffer,
				trade_offer_create_params: data.tradeOfferCreateParams,
				tradeoffermessage: data.tradeOfferMessage
			}

		try 
		{
			const fetchQueuePriority = 0;
			const headers = {"X-Referer": data.partnerTradeUrl}; // X-Referer заменяется на Referer в ExtensionManager.js
			const response = await postForm(url, params, headers, fetchQueuePriority);
			const json = await response.json();
			json.assetIds = data.assetIds;
			json.siteOrderIds = data.siteOrderIds;
			json.message = data.tradeOfferMessage;

			return json;
		} 
		catch(e) 
		{
			console.error(e);
			return JSON.parse(e.message);
		}
	}
	/*
	response example
	{
	  "response": {
	    "offer": {
	      "tradeofferid": "5343876036",
	      "accountid_other": 399812896,
	      "message": "AMWS",
	      "expiration_time": 1660068755,
	      "trade_offer_state": 10,
	      "items_to_give":
	      [
	        {
	          "appid": 570
	          "contextid": "2"
	          "assetid": "23876538043"
	          "classid": "1723111174"
	          "instanceid": "57949762"
	          "amount" "1"
	          "missing": false
	          "est_usd": "1"
	        }
	      ]
	      "is_our_offer": true,
	      "time_created": 1658859155
	      "time_updated": 1658859184
	      "from_real_time_trade": false
	      "escrow_end_date": 0,
	      "confirmation_method": 2
	  }
	}
	*/
	async getTradeOffer(tradeOfferId) {
		const url = 'https://api.steampowered.com/IEconService/GetTradeOffer/v1/?tradeofferid='+tradeOfferId+'&language=en&key='+this.accountApiKey;
		const response = await fetchRateLimited(url);
		return await response.json();
	}
	async cancelTradeOffer(tradeOfferId) {
		const url = 'https://steamcommunity.com/tradeoffer/'+tradeOfferId+'/cancel',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				sessionid: sessionidCookie.value,
			};
		
		const response = await postForm(url, params);

		return response.json();
	}
	async registerApiKey() 
	{
		const url = 'https://steamcommunity.com/dev/registerkey',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				domain: 'localhost',
				agreeToTerms: 'agreed',
				sessionid: sessionidCookie.value,
				Submit: 'Зарегистрировать',
			};

		const response = await postForm(url, params);

		return parseApiKey(response, this);
	}
	async getActiveSentOffers() 
	{
		const key = this.accountApiKey;
		const url = 'https://api.steampowered.com/IEconService/GetTradeOffers/v1/?key='+key+'&get_sent_offers=1&active_only=1';
		const response = await fetchRateLimited(url);
		return await response.json();
	}
	async isOfferAlreadyCreated(inputAssetids) 
	{
		const activeSentOffers = await this.getActiveSentOffers();

		for(const i in activeSentOffers) {
			const offer = activeSentOffers[i];
			for(const j in offer.items_to_give) {
				const steamTradeItem = offer.items_to_give[j];
				if(
					inputAssetids.includes(steamTradeItem.assetid)
				&&  ![
						SteamTradeOfferState.Expired, // 5
						SteamTradeOfferState.Canceled, // 6
						SteamTradeOfferState.Declined, // 7
						SteamTradeOfferState.CanceledBySecondFactor // 10
					].includes(offer.trade_offer_state)
				) {
					return true;
				}
			}
		}

		return false;
	}
}

async function parseApiKey(response, self) {
	const text = await response.text();
	const matches = text.match(/<p>.+: ([A-Z0-9]+)<\/p>/);
	if(matches != null) {
		self.accountApiKey = matches[1];
		return true;
	}
	else
		return false;
}

async function postForm(url, params, headers = {}, fetchQueuePriority = 1) {
	const fd = new FormData();
	for(const key in params) fd.set(key, params[key]);
	const body = urlencodeFormData(fd)
	const response = await fetchRateLimited(url, {
		method: 'POST',
		headers: Object.assign(headers,{
			'Content-Type': 'application/x-www-form-urlencoded'
		}),
		body
	}, fetchQueuePriority);
	if(!response.ok) {
		const text = await response.text();
		throw Error(text);
	}
	return response;
}

function urlencodeFormData(fd) {
    let s = '';
    function encode(s){ return encodeURIComponent(s).replace(/%20/g,'+'); }
    for(const pair of fd.entries()){
        if(typeof pair[1]=='string'){
            s += (s?'&':'') + encode(pair[0])+'='+encode(pair[1]);
        }
    }
    return s;
}

// todo сделать 3 ретрая
function fetchRateLimited(url, params = null, queuePriority = 1) {
	return throttle1s(queuePriority).then(() => fetch(url, params));
}
